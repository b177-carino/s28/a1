// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then((json) => console.log(json));



// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
/*fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(json => {json.map((item) => {
    console.log(item.title) 
  })
})*/

// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		"Content-type": "application/json"
	},
	body: JSON.stringify({
		title: "Created a to do list item",
		id: 1,
		completed: false
	}),
}) 

.then((response) => response.json())
.then((json) => console.log(json))


// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
}) 